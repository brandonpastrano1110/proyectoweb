const firebaseConfig = {
    apiKey: "AIzaSyACyJVIyCkXJ0LGzxHBilkHpB8uj3orq5k",
    authDomain: "proyectoweb-f9bfc.firebaseapp.com",
    projectId: "proyectoweb-f9bfc",
    storageBucket: "proyectoweb-f9bfc.appspot.com",
    messagingSenderId: "334171937140",
    appId: "1:334171937140:web:385befcdd0aeef5651b421",
    databaseURL: "https://proyectoweb-f9bfc-default-rtdb.firebaseio.com/"
};
firebase.initializeApp(firebaseConfig);
const storage = firebase.storage();
const storageRef = storage.ref();
const database = firebase.database();
const productosRef = database.ref('Productos');

function subirImagen() {
    const fileInput = document.getElementById('fileInput');
    const archivo = fileInput.files[0];
    const urlInput = document.getElementById('urlInput');
    const nombreProducto = document.getElementById('nombreProducto').value;
    const codigoProducto = document.getElementById('codigoProducto').value;
    const descripcionProducto = document.getElementById('descripcionProducto').value;
    const statusProducto = document.getElementById('statusProducto').value;
    if (archivo) {
        if (archivo.type === 'image/jpeg' || archivo.type === 'image/png') {
            const imagenRef = storageRef.child(`imagenes/${archivo.name}`);

            imagenRef.put(archivo).then((snapshot) => {
                alert('Imagen subida con éxito.');

                snapshot.ref.getDownloadURL().then((url) => {
                    urlInput.value = url;

                    const productosRef = database.ref('Productos');
                    productosRef.push({
                        nombre: nombreProducto,
                        codigo: codigoProducto,
                        descripcion: descripcionProducto,
                        status: statusProducto,
                        imagenURL: urlInput.value
                    });
                }).catch((error) => {
                    alert('Error al obtener la URL de descarga: ' + error.message);
                });
            }).catch((error) => {
                alert('Error al subir la imagen: ' + error.message);
            });
        } else {
            alert('Por favor, selecciona una imagen en formato .jpg, .jpeg o .png.');
        }
        limpiar();
    } else {
        alert('Por favor, selecciona un archivo.');
    }
}
productosRef.on('value', (snapshot) => {
    const productosContainer = document.getElementById('productosContainer');
    productosContainer.innerHTML = '';

    snapshot.forEach((productoSnapshot) => {
        const producto = productoSnapshot.val();
        if (producto.status === 'activo') {
            const productoElement = document.createElement('div');
            productoElement.innerHTML = `
                <h3>${producto.nombre}</h3>
                <p>Código: ${producto.codigo}</p>
                <p>Descripción: ${producto.descripcion}</p>
                <p>Estado: ${producto.status}</p>
                <img src="${producto.imagenURL}" alt="Imagen del producto">
            `;

            productosContainer.appendChild(productoElement);
        }
    });
})
function mostrarImagen() {
    const fileInput = document.getElementById('fileInput');
    const archivo = fileInput.files[0];
    const urlInput = document.getElementById('urlInput');
    const imagenContainer = document.getElementById('imagenContainer');

    if (archivo) {
        const imagen = document.createElement('img');
        const url = URL.createObjectURL(archivo);
        imagen.src = url;
        imagenContainer.innerHTML = '';
        imagenContainer.appendChild(imagen);
        urlInput.value = url;
    }
}
function limpiar() {
    document.getElementById('nombreProducto').value = '';
    document.getElementById('codigoProducto').value = '';
    document.getElementById('descripcionProducto').value = '';
    document.getElementById('statusProducto').value = '';
    document.getElementById('fileInput').value = '';
    document.getElementById('urlInput').value = '';
    document.getElementById('imagenContainer').innerHTML = '';
}
function cerrarSesion() {
    window.location.href = "/index.html";
}
function buscarProducto() {
    const codigo = prompt('Ingrese el codigo a buscar: ');
    if (codigo) {
        productosRef.orderByChild('codigo').equalTo(codigo).once('value')
            .then((snapshot) => {
                if (snapshot.exists()) {
                    const productoKey = Object.keys(snapshot.val())[0];
                    const producto = snapshot.val()[productoKey];

                    // Mostrar los detalles del producto en los inputs
                    document.getElementById('codigoProducto').value = producto.codigo;
                    document.getElementById('nombreProducto').value = producto.nombre;
                    document.getElementById('descripcionProducto').value = producto.descripcion;
                    document.getElementById('statusProducto').value = producto.status;
                    document.getElementById('urlInput').value = producto.imagenURL;
                } else {
                    alert('Producto no encontrado.');
                }
            })
            .catch((error) => {
                console.error('Error al buscar el producto:', error);
            });
    }
}

function deshabilitarProducto() {
    const codigo = prompt('Ingrese el código del producto que desea deshabilitar:');
    if (codigo) {
      productosRef.orderByChild('codigo').equalTo(codigo).once('value')
        .then((snapshot) => {
          if (snapshot.exists()) {
            snapshot.forEach((productoSnapshot) => {
              const productoKey = productoSnapshot.key;
              productosRef.child(productoKey).update({ status: 'Inactivo' })
                .then(() => {
                  alert('Producto deshabilitado correctamente.');
                })
                .catch((error) => {
                  alert('Error al deshabilitar el producto: ' + error.message);
                });
            });
          } else {
            alert('Producto no encontrado.');
          }
        })
        .catch((error) => {
          console.error('Error al buscar el producto:', error);
        });
    }
  }
  
// Función para actualizar un producto por su código
function actualizarProducto() {
    const codigo = prompt('Código del producto que desea actualizar:');
    if (codigo) {
        const fileInput = document.createElement('input');
        fileInput.type = 'file';
        fileInput.accept = 'image/*';
        fileInput.onchange = (event) => {
            const file = event.target.files[0];
            if (file) {
                const storageRef = storage.ref(`imagenes/${file.name}`);
                storageRef.put(file).then(() => {
                    storageRef.getDownloadURL().then((url) => {
                        const nuevoNombre = prompt('Ingrese el nuevo nombre del producto:');
                        const nuevaDescripcion = prompt('Ingrese la nueva descripcion del producto:');

                        productosRef.orderByChild('codigo').equalTo(codigo).once('value')
                            .then((snapshot) => {
                                if (!snapshot.exists()) {
                                    alert('Producto no encontrado')
                                } else {
                                    snapshot.forEach((productoSnapshot) => {
                                        const productoKey = productoSnapshot.key;
                                        productosRef.child(productoKey).update({
                                            nombre: nuevoNombre,
                                            descripcion: nuevaDescripcion,
                                            url: url // Utilizar la URL de la imagen subida
                                        }).then(() => {
                                            alert('Producto actualizado correctamente.');
                                        }).catch((error) => {
                                            alert('Error al actualizar el producto: ' + error.message);
                                        });
                                    });
                                }
                            })
                            .catch((error) => {
                                console.error('Error al buscar el producto:', error);
                            });
                    }).catch((error) => {
                        alert('Error al obtener la URL de descarga de la imagen: ' + error.message);
                    });
                }).catch((error) => {
                    alert('Error al subir la imagen: ' + error.message);
                });
            }
        };
        fileInput.click();
    }
}  