// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyACyJVIyCkXJ0LGzxHBilkHpB8uj3orq5k",
  authDomain: "proyectoweb-f9bfc.firebaseapp.com",
  projectId: "proyectoweb-f9bfc",
  storageBucket: "proyectoweb-f9bfc.appspot.com",
  messagingSenderId: "334171937140",
  appId: "1:334171937140:web:385befcdd0aeef5651b421"
}; 
const app = initializeApp(firebaseConfig);
const btnEnviar = document.getElementById('btnEnviar');

btnEnviar.addEventListener("click", function (event) {
  event.preventDefault();
  const email = document.getElementById('txtEmail').value;
  const password = document.getElementById('txtPassword').value; 
  const auth = getAuth();
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;
      alert(`Usuario autenticado: ${user.email}`);
      window.location.href = "/html/storage.html"; 
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(`Error durante la autenticación: ${errorMessage}`);
    });

});
